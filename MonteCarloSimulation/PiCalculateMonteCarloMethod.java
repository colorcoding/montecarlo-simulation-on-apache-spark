package com.sparkpractice;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

public class PiCalculateMonteCarloMethod {

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("PiCalculate").setMaster("local[*]");

		JavaSparkContext context = new JavaSparkContext(conf);

		long startTime = System.currentTimeMillis();
		// Create a List to create RDD of integers for calculation
		int slices = (args.length == 1) ? Integer.parseInt(args[0]) : 10;
		int n = 1000000;
		List<Integer> l = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			l.add(i);
		}

		// Create RDD from a collection
		JavaRDD<Integer> intRdd = context.parallelize(l, 2);

		int count = intRdd.map(input -> {
			double x = Math.random() * 2 - 1;
			double y = Math.random() * 2 - 1;
			return ((x * x) + (y * y) <= 1) ? 1 : 0;
		}).reduce((i1, i2) -> i1 + i2);

		System.out.println("Pi is roughly " + 4.0 * count / n);
		System.out.println("Time Elapsed: " + (System.currentTimeMillis() - startTime) + " msec");
	}
}
