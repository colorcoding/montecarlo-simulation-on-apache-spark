package com.sparkpractice;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

public class PiCalculateLeibnizMethod {

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("PiCalculateLiebniz").setMaster("local[*]");

		JavaSparkContext context = new JavaSparkContext(conf);

		long startTime = System.currentTimeMillis();
		// Create a List to create RDD of integers for calculation
		int slices = (args.length == 1) ? Integer.parseInt(args[0]) : 10;
		int n = 1000000;
		List<Integer> l = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			l.add(i);
		}

		// Create RDD from a collection
		JavaRDD<Integer> intRdd = context.parallelize(l, 2);

		// Calculate Summation using Leibniz formula
		// ∑ n=0 to n->∞ ((-1 ^ n)/ (2n + 1)) = pi/4
		double summation = intRdd.map(input -> {
			double num = ((input % 2 == 0) ? 1 : -1);
			double den = (2*input) + 1;
			return num/den;
		}).reduce((i1, i2) -> i1 + i2);

		System.out.println("Pi is roughly " + 4.0 * summation);
		System.out.println("Time Elapsed: " + (System.currentTimeMillis() - startTime) + " msec");
	}
}
