<h2>Calculating value of Pi </h2>

Apache Spark is known for its capability in fast analysis and processing batch and real time data by using RDD (Resilient Distributed Data, basically a DAG) and taking advantage of cluster computing.
But it can also be used for simulation purposes, due to its inherent nature of distributing the workloads.

Calculating value of Pi is a very basic program with Spark -

<h3> 1. using Monte Carlo simulation </h3>
To understand the MonteCarlo simulation method to calculate Pi, refer to link http://demonstrations.wolfram.com/MonteCarloEstimateForPi/

Area of a circle/ Area of a Square = (no of points generated inside the circle / no of points generated outside the circle)

i.e. => pi = 4 * (no of points generated inside the circle / no of points generated outside the circle)

<h3> 2. Using Leibniz Formula</h3>
This is just to check the accuracy of Pi and the time taken by a predefined formula like Leibniz.

The formula is =>
∑ n=0 to n->∞ ((-1 ^ n)/ (2n + 1)) = pi/4

<h3> Results </h3>
I observed that CPU activity was at peak while the program ran, although it was for couple of secs usually

<table>
<thead>
<td>Method</td>
<td>Runs/ simulation</td>
<td>Slices</td>
<td>Time Elapsed</td>
<td>Value of Pi</td>
</thead>
<tbody>
<tr>
<td>MonteCarlo</td>
<td>100000</td>
<td>10</td>
<td>1386 msec</td>
<td>3.13778</td>
<tr>
<tr>
<td>MonteCarlo</td>
<td>1000000</td>
<td>10</td>
<td>2204 msec</td>
<td>3.142464</td>
<tr>
<tr>
<td>Leibniz</td>
<td>100000</td>
<td>10</td>
<td>2085 msec</td>
<td>3.1415916535897197</td>
<tr>
<tr>
<td>Leibniz</td>
<td>1000000</td>
<td>10</td>
<td>1515 msec</td>
<td>3.1415826535897846</td>
<tr>
</tbody>
</table>


P.S. The above program is run locally on Eclipse and not on spark cluster.